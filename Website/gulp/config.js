﻿var notifier = require('node-notifier');
var argv = require('yargs').argv;

module.exports = (function () {
    var projectName = "heyday.frontend";

    var projectPath = "./";
    var bowerPath = projectPath + "vendor/bower";
    var distPath = projectPath + "dist";
    var cleanPaths = [distPath];
    var preprocessor = "less";

    return {
        // ------------- Bundles -------------
        bundles: [
            {
                name: "vendor",
                ignorePlugins: ["jscs", "jshint", "watch"], // add "minify", to ignore minifaction on a bundle
                scripts: [
                    bowerPath + "/jquery/dist/jquery.js",
                    bowerPath + "/svg4everybody/dist/svg4everybody.js",
                    bowerPath + "/js-cookie/src/js.cookie.js"
                ]
            },
            {
                name: "master",
                ignorePlugins: ["jscs"],
                scripts: [

                    /*
                        OBS: I have removed the inclusion of single script files to test if this is a better
                        way. 

                        Pros: One less step when creating new *.js files.

                        Cons: All .js files are included, without the option to omit single files or dictate
                        load sequence. 

                        projectPath + "scripts/base/heyday.visible.js",
                        projectPath + "scripts/base/heyday.timing.js",
                        projectPath + "scripts/base/heyday.touch.js",
                        projectPath + "scripts/components/heyday.lazyload.js",
                        projectPath + "scripts/components/current_project.sampleFunction.js",
                    */

                    projectPath + "scripts/*/*.js",    
                    projectPath + "scripts/master.js"

                ],
                styles: [projectPath + preprocessor + "/master." + preprocessor],
                images: [projectPath + "images/**/*.{jpg,png,svg,gif}"],
                icons: [projectPath + "icons/**/*.svg"]
            }
        ],


        // ------------- Styles -------------
        stylesDist: distPath + "/css",
        stylesVendorPrefixes: [
            "last 2 version",
            "safari 5",
            "ie 9",
            "opera 12.1",
            "ios 8",
            "android 4"
        ],

        // ------------- Scripts -------------
        scriptsDist: distPath + "/scripts",

        // ------------- Icons ---------------
        iconsDist: distPath,
        spriteConfig: {
            shape : {
                // Set maximum dimensions
                dimension       : {
                    maxWidth    : 32,
                    maxHeight   : 32
                }
            },
            mode : {
                view : {
                    bust : false,
                    render : {
                        less : true
                    },
                    dest : 'icons',
                    sprite : 'icons-css.svg'
                },
                symbol : {
                    dest : 'icons',
                    sprite : 'icons.svg'
                }
            }
        },

        // ------------- Fonts -------------
        fontsDist: distPath + "/fonts",

        // ------------- Images -------------
        imagesDist: distPath + "/images",
        imagesOptimizationLevel: 5,
        imagesProgressive: true,
        imagesInterlaced: true,

        // ------------- Livereload ---------
        livereloadPort: 35729,
        livereloadPaths: [
            "./dist/scripts/*.js",
            "./dist/css/*.css"
        ],

        // ------------- Watch -------------
        watchImages: [ projectPath + "images/**/*", projectPath + '!images/icons/*' ],
        watchIcons: [ projectPath + "icons/**/*" ],
        watchFonts: [ projectPath + "fonts/*" ],
        watchScripts: [
            projectPath + "scripts/**/*.js",
            projectPath + "vendor/**/*.js"
        ],
        watchStyles: [
            projectPath + preprocessor + "/**/*." + preprocessor,
            projectPath + "vendor/**/*.{less, scss}"
        ],

        // ------------- Copy on build --------
        buildCopy: [{
            from: projectPath + "fonts/**/*",
            to: distPath  + "/fonts"
        }],


        // ------------- Tasks -------------
        loadTasks: [
            "bower", "styles", "scripts",
            "images", "icons", "copy",
            "watch", "build"
        ],
        buildTasks: [
            "styles", "scripts",
            "images", "icons", "copy"
        ],

        // ------------- Return Paths -------------
        projectPath: projectPath,
        bowerPath: bowerPath,
        cleanPaths: cleanPaths,
        preprocessor: preprocessor,

        // ---------- Errorhandler ------
        errorHandler: function(taskName)
        {
            return function (e) {
                notifier.notify({
                    "title": taskName,
                    "message": "An error occured in the " + e.plugin + " plugin."
                });
                console.log(e.message);
                this.emit("end");
            };
        }
    }
})();