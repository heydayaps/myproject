/* Heyday
05/2017 - Mads Rode
********************************************************************/

var heyday = heyday || {};
var current_project = current_project || {};


/*
	Please note that master.js is meant as an pickup file for all the 
	various components in the projects. Therefor it is not viable to
	write functions directly in this file.  

	When building scripts please follow the object syntax describet in current_project.sampleFunction.js
	and avoid implementing other $(function() {}) calls than the one found in master.js

	Whenever creating and importing new .js files, please remember to run 
	the command "gulp" and if needed re-innitiate live review with "gulp watch"
*/


// Document ready
$(function () {
    // OBS - Please delete this function once the project has been renamed
	if (typeof current_project !== 'undefined') {
        $('body').prepend(
        	  '<div class="debug"' 
        	+ '		style="'
        	+ '			text-align:center;		'
        	+ '			font-weight:bold;		'
        	+ '			background:#eee;		'
        	+ '			color:#666;				'
        	+ '			border:solid 5px #666;	'
        	+ '			padding:25px;			'
        	+ '			margin-bottom:75px;		'
        	+ '			border-radius:15px;		'
        	+ '		"'
        	+ '>'
        	+ '		Please remember to rename the "current_project" object in master.js file '
        	+ ' 	& current_project.sampleFunction.js, to properbly reflect the project.'
        	+ '		Once renamed, this function can safely be deleted.'
        	+ '</div>'
    	);
    }

	current_project.sampleFunction.init(); 


    
    if(isTouch.any()) {
        console.log('This is a touch device');
    } else {
        console.log('This is NOT a touch device');
    }

}); // Document ready end


$(window).load(function () {    
    heyday.lazyload.lazyloadBackground();
});

/*
    resize & scroll are using the timing function in 
    order to reduce the amount of event requests
*/

$(window).resize(heyday.timing.debounce(function () {
    heyday.lazyload.lazyloadBackground();
}, 50));


$(window).scroll(heyday.timing.debounce(function () { 
    heyday.lazyload.lazyloadBackground();
}, 50));