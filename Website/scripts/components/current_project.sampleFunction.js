var current_project = current_project || {};

current_project.sampleFunction = new function () {

    this.init = function () {
        sampleFunction();
    }

    function sampleFunction() {
        console.log('Running')
        if($('.test-visible').length) {
            var $testClass = $('.test-visible');

            console.log('class found')

            $testClass.each(function() {
                var $current = $(this);

                if($current.visible) {
                    $current.removeClass('test-visible');
                    console.log('class added')
                }
            });
        }
    }
};

