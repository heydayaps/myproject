var heyday = heyday || {};

/**
    These function allows for the lazyloading of images & background images,
    relative to the current breakpoint.

    This can also be implemented as progressive lazyloading by 
    adding these to the resize & scroll events.
**/

heyday.lazyload = new function () {

    this.lazyloadBackground = function () {
        if ($('.lazyload-background').length) {

            $('.lazyload-background').each(function () {
                var $current = $(this);
                var src = $current.data('src');
                var srcMd = src;
                var srcSm = src;
                var srcMs = src;
                var srcXs = src;

                var width = $(window).innerWidth();

                if ($current.attr('data-src-md')) {
                    srcMd = $current.data('src-md');
                }

                if ($current.attr('data-src-sm')) {
                    srcSm = $current.data('src-sm');
                }

                if ($current.attr('data-src-ms')) {
                    srcMs = $current.data('src-ms');
                }

                if ($current.attr('data-src-xs')) {
                    srcXs = $current.data('src-xs'); 
                }

                var imgSrc = src;

                if (width > 0 && width < 480 && srcXs != src) {
                    imgSrc = srcXs;
                }

                if (width > 480 && width < 768 && srcMs != src) {
                    imgSrc = srcMs;
                }
                if (width >= 768 && width < 992 && srcSm != src) {
                    imgSrc = srcSm;
                }
                if (width >= 992 && width < 1200 && srcMd != src) {
                    imgSrc = srcMd;
                }
                if (width >= 1200) {
                    //imgSrc = src;
                }

                $current.css('background-image', 'url(' + imgSrc + ')');
            });
        }
    }

    this.lazyloadImage = function () {
        if ($('.lazyload-image').length) {
            $('.lazyload-image').each(function () {
                var $current = $(this);
                var src = $current.data('src');
                var srcMd = src;
                var srcSm = src;
                var srcMs = src;
                var srcXs = src;

                var width = $(window).innerWidth();

                if ($current.attr('data-src-md')) {
                    srcMd = $current.data('src-md');
                }

                if ($current.attr('data-src-sm')) {
                    srcSm = $current.data('src-sm');
                }

                if ($current.attr('data-src-ms')) {
                    srcMs = $current.data('src-ms');
                }

                if ($current.attr('data-src-xs')) {
                    srcXs = $current.data('src-xs');
                }

                var imgSrc = src;

                if (width > 0 && width < 480 && srcXs != src) {
                    imgSrc = srcXs;
                }

                if (width > 480 && width < 768 && srcMs != src) {
                    imgSrc = srcMs;
                }
                if (width >= 768 && width < 992 && srcSm != src) {
                    imgSrc = srcSm;
                }
                if (width >= 992 && width < 1200 && srcMd != src) {
                    imgSrc = srcMd;
                }
                if (width >= 1200) {
                    //imgSrc = src;
                }

                $current.prop('src', imgSrc);

            });
        }
    }
};








